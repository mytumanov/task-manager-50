package ru.mtumanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import java.util.Comparator;
import java.util.List;

public interface IDtoUserOwnedRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {

    void add(@NotNull M entity) throws AbstractException;

    void clear(@NotNull String userId);

    @NotNull
    M update(@NotNull M entity) throws AbstractException;

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);

    void removeById(@NotNull String userId, @NotNull String id);

    boolean existById(@NotNull String userId, @NotNull String id);

}
