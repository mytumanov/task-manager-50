package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.api.endpoint.IProjectEndpoint;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.dto.request.project.*;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.response.project.*;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.marker.SoapCategory;
import ru.mtumanov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final static IPropertyService propertyService = new PropertyService();

    @NotNull
    private final static String host = propertyService.getServerHost();

    @NotNull
    private final static String port = propertyService.getServerPort();

    @NotNull
    private final static IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final static String login = "NOT_COOL_USER";

    @NotNull
    private final static String password = "Not Cool";

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private static String token;

    @BeforeClass
    public static void login() {
        @NotNull final UserLoginRq request = new UserLoginRq(login, password);
        @NotNull final UserLoginRs response = authEndpoint.login(request);
        token = response.getToken();
    }

    @After
    public void clearData() {
        projectEndpoint.projectClear(new ProjectClearRq(token));
    }

    @Before
    public void initData() {
        projectList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectCreateRs response = projectEndpoint.projectCreate(new ProjectCreateRq(token, "TEST Project " + i, "Test description " + i));
            projectList.add(response.getProject());
        }
    }

    @Test
    public void testProjectChangeStatusById() {
        for (@NotNull final ProjectDTO project : projectList) {
            @NotNull final ProjectChangeStatusByIdRs response = projectEndpoint
                    .projectChangeStatusById(new ProjectChangeStatusByIdRq(token, project.getId(), Status.IN_PROGRESS));
            assertTrue(response.getSuccess());
            assertNotEquals(project.getStatus(), response.getProject().getStatus());
        }
    }

    @Test
    public void testErrProjectChangeStatusById() {
        @NotNull ProjectChangeStatusByIdRs response;
        response = projectEndpoint
                .projectChangeStatusById(new ProjectChangeStatusByIdRq("", UUID.randomUUID().toString(), Status.IN_PROGRESS));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = projectEndpoint
                .projectChangeStatusById(new ProjectChangeStatusByIdRq(token, "", Status.IN_PROGRESS));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectClear() {
        @NotNull ProjectShowByIdRs projectRs;
        projectRs = projectEndpoint.projectShowById(new ProjectShowByIdRq(token, projectList.get(0).getId()));
        assertNotNull(projectRs.getProject());

        @NotNull final ProjectClearRs response = projectEndpoint.projectClear(new ProjectClearRq(token));
        assertTrue(response.getSuccess());

        projectRs = projectEndpoint.projectShowById(new ProjectShowByIdRq(token, projectList.get(0).getId()));
        assertNull(projectRs.getProject());
    }

    @Test
    public void testErrProjectClear() {
        @NotNull final ProjectClearRs response = projectEndpoint.projectClear(new ProjectClearRq());
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectCompleteById() {
        for (@NotNull final ProjectDTO project : projectList) {
            @NotNull final ProjectCompleteByIdRs response = projectEndpoint.projectCompleteById(new ProjectCompleteByIdRq(token, project.getId()));
            assertEquals(Status.COMPLETED, response.getProject().getStatus());
            assertTrue(response.getSuccess());
        }
    }

    @Test
    public void testErrProjectCompleteById() {
        @NotNull ProjectCompleteByIdRs response;
        response = projectEndpoint.projectCompleteById(new ProjectCompleteByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = projectEndpoint.projectCompleteById(new ProjectCompleteByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectCreate() {
        @NotNull final String name = "Create project";
        @NotNull final String description = "description";
        @NotNull final ProjectCreateRs response = projectEndpoint
                .projectCreate(new ProjectCreateRq(token, name, description));
        assertEquals(name, response.getProject().getName());
        assertEquals(description, response.getProject().getDescription());
        assertEquals(Status.NOT_STARTED, response.getProject().getStatus());
    }

    @Test
    public void testErrProjectCreate() {
        @NotNull final String name = "Create project";
        @NotNull final String description = "description";
        @NotNull ProjectCreateRs response;

        response = projectEndpoint.projectCreate(new ProjectCreateRq("", name, description));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = projectEndpoint.projectCreate(new ProjectCreateRq(token, "", description));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectList() {
        @NotNull final ProjectListRs response = projectEndpoint.projectList(new ProjectListRq(token, null));
        assertEquals(projectList, response.getProjects());
    }

    @Test
    public void testErrProjectList() {
        @NotNull final ProjectListRs response = projectEndpoint.projectList(new ProjectListRq("", null));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectRemoveById() {
        for (@NotNull final ProjectDTO project : projectList) {
            projectEndpoint.projectRemoveById(new ProjectRemoveByIdRq(token, project.getId()));
            @NotNull final ProjectShowByIdRs response = projectEndpoint.projectShowById(new ProjectShowByIdRq(token, project.getId()));
            assertNull(response.getProject());
        }
    }

    @Test
    public void testErrProjectRemoveById() {
        @NotNull final ProjectRemoveByIdRs response = projectEndpoint.projectRemoveById(new ProjectRemoveByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectShowById() {
        for (@NotNull final ProjectDTO project : projectList) {
            @NotNull final ProjectShowByIdRs response = projectEndpoint.projectShowById(new ProjectShowByIdRq(token, project.getId()));
            assertEquals(project, response.getProject());
        }
    }

    @Test
    public void testErrProjectShowById() {
        @NotNull final ProjectShowByIdRs response = projectEndpoint.projectShowById(new ProjectShowByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectStartById() {
        for (@NotNull final ProjectDTO project : projectList) {
            @NotNull final ProjectStartByIdRs response = projectEndpoint.projectStartById(new ProjectStartByIdRq(token, project.getId()));
            assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
        }
    }

    @Test
    public void testErrProjectStartById() {
        @NotNull final ProjectStartByIdRs response = projectEndpoint.projectStartById(new ProjectStartByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testProjectUpdateById() {
        for (final ProjectDTO project : projectList) {
            @NotNull final String name = project.getName() + " TEST";
            @NotNull final String description = project.getDescription() + " TEST";
            @NotNull final ProjectUpdateByIdRs response = projectEndpoint.projectUpdateById(new ProjectUpdateByIdRq(token, project.getId(), name, description));
            assertEquals(name, response.getProject().getName());
            assertEquals(description, response.getProject().getDescription());
        }
    }

    @Test
    public void testErrProjectUpdateById() {
        @NotNull final ProjectUpdateByIdRs response = projectEndpoint
                .projectUpdateById(new ProjectUpdateByIdRq(token, "123", "name", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

}
